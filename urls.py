from django.urls import path
from .views import viewPrincipal,viewSecundario

urlpatterns = [
    path("principal/", viewPrincipal, name = "principal"),
    path("secundario/", viewSecundario, name = "secundario")
]